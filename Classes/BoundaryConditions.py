from enum import Enum

class BoundaryLocation(Enum):
    """Enumeration class defining boundary condition locations"""
    WEST = 1
    EAST = 2

class DirichletBc:
    """Class defining a Dirichlet boundary condition"""

    def __init__(self, phi, grid, value, loc):
        """Constructor
            phi ..... field variable array
            grid .... grid
            value ... boundary value
            loc ..... boundary location
        """
        self._phi = phi
        self._grid = grid
        self._value = value
        self._loc = loc

    def reverseLoc(self):
        self._loc = BoundaryLocation.EAST if self._loc==BoundaryLocation.WEST else BoundaryLocation.WEST

    def value(self):
        """Return the boundary condition value"""
        return self._value

    def coeff(self):
        """Return the linearization coefficient"""
        return 0

    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            self._phi[0] = self._value
        elif self._loc is BoundaryLocation.EAST:
            self._phi[-1] = self._value
        else:
            raise ValueError("Unknown boundary location")

class NeumannBc:
    """Class defining a Neumann boundary condition"""

    def __init__(self, phi, grid, gradient, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            gradient ... gradient at cell adjacent to boundary
            loc ........ boundary location
        """
        self._phi = phi
        self._grid = grid
        self._gradient = gradient
        self._loc = loc

    def reverseLoc(self):
        self._loc = BoundaryLocation.EAST if self._loc==BoundaryLocation.WEST else BoundaryLocation.WEST

    def value(self):
        """Return the boundary condition value"""
        if self._loc is BoundaryLocation.WEST:
            return self._phi[1] - self._gradient*self._grid.dx_WP[0]
        elif self._loc is BoundaryLocation.EAST:
            return self._phi[-2] + self._gradient*self._grid.dx_PE[-1]
        else:
            raise ValueError("Unknown boundary location")

    def coeff(self):
        """Return the linearization coefficient"""
        return 1

    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            self._phi[0] = self._phi[1] - self._gradient*self._grid.dx_WP[0]
        elif self._loc is BoundaryLocation.EAST:
            self._phi[-1] = self._phi[-2] + self._gradient*self._grid.dx_PE[-1]
        else:
            raise ValueError("Unknown boundary location")


class RobinBc:
    """Class defining a Neumann boundary condition"""

    def __init__(self, phi, grid, ho, To, k, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            gradient ... gradient at cell adjacent to boundary
            loc ........ boundary location
        """
        self._phi = phi
        self._grid = grid
        self._ho = ho
        self._To = To
        self._loc = loc
        self._k = k

    def reverseLoc(self):
        self._loc = BoundaryLocation.EAST if self._loc==BoundaryLocation.WEST else BoundaryLocation.WEST

    def value(self):
        """Return the boundary condition value"""
        if self._loc==BoundaryLocation.WEST:
            num = self._phi[1] + self._grid.dx_WP[0]*(self._ho/self._k)*self._To
            denom = 1+self._grid.dx_WP[0]*(self._ho/self._k)
            return num/denom
        elif self._loc == BoundaryLocation.EAST:
            num = self._phi[-2] + self._grid.dx_PE[-1]*(self._ho/self._k)*self._To
            denom = 1+self._grid.dx_PE[-1]*(self._ho/self._k)
            return num/denom
        else:
            raise ValueError("Unknown boundary condition")

    def coeff(self):
        """Return the linearization coefficient"""
        if self._loc == BoundaryLocation.WEST:
            return 1/(1+self._grid.dx_WP[0]*self._ho/self._k)
        elif self._loc == BoundaryLocation.EAST:
            return 1/(1+self._grid.dx_PE[-1]*self._ho/self._k)

    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc==BoundaryLocation.WEST:
            num = self._phi[1] + self._grid.dx_WP[0]*(self._ho/self._k)*self._To
            denom = 1+self._grid.dx_WP[0]*(self._ho/self._k)
            self._phi[0] = num/denom
        elif self._loc == BoundaryLocation.EAST:
            num = self._phi[-2] + self._grid.dx_PE[-1]*(self._ho/self._k)*self._To
            denom = 1+self._grid.dx_PE[-1]*(self._ho/self._k)
            self._phi[-1] = num/denom
        else:
            raise ValueError("Unknown boundary condition")



class ExtrapolatedBc:
    """Class defining an extrapolated boundary condition"""

    def __init__(self, phi, grid, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            loc ........ boundary location
        """
        self._phi = phi
        self._grid = grid
        self._loc = loc

    def value(self):
        """Return the boundary condition value"""
        if self._loc is BoundaryLocation.WEST:
            return self._phi[1]+self._grid.dx_WP[0]*(self._phi[1]-self._phi[2])/self._grid.dx_WP[1]
        elif self._loc is BoundaryLocation.EAST:
            return self._phi[-2]+self._grid.dx_PE[-1]*(self._phi[-2]-self._phi[-3])/self._grid.dx_PE[-2]
        else:
            raise ValueError("Unknown boundary location")

    def coeff(self):
        """Return the linearization coefficient"""
        if self._loc is BoundaryLocation.WEST:
            return 1.5
        elif self._loc is BoundaryLocation.EAST:
            return 1.5
        else:
            raise ValueError("Unknown boundary location")

    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            #pass Fill in expression below
            self._phi[0] = self._phi[1]+self._grid.dx_WP[0]*(self._phi[1]-self._phi[2])/self._grid.dx_WP[1]
        elif self._loc is BoundaryLocation.EAST:
            #pass # Fill in expression below
            self._phi[-1] = self._phi[-2]+self._grid.dx_PE[-1]*(self._phi[-2]-self._phi[-3])/self._grid.dx_PE[-2]
        else:
            raise ValueError("Unknown boundary location")

